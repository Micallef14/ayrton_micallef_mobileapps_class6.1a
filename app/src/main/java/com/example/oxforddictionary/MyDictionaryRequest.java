package com.example.oxforddictionary;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MyDictionaryRequest extends AsyncTask<String,Integer,String> {
    TextView t1;
    EditText e1;
    String myUrl;
    Context context;


    MyDictionaryRequest (Context context , TextView t1)
    {
        this.context = context;
        this.t1 = t1;
    }

    @Override
    protected String doInBackground(String... params)
    {
        String word = params[0];

        String url = "https://od-api.oxforddictionaries.com:443/api/v2/entries/"+ "en" + "/" + word;
        final String app_id = "3e282072";
        final String app_key = "9ddb205a602be046627ce1c490ea7756";

        try {

            URL myUrl = new URL(url);
            HttpsURLConnection urlConnection = (HttpsURLConnection) myUrl.openConnection();
            urlConnection.setRequestProperty("Accept","application/json");
            urlConnection.setRequestProperty("app_id",app_id);
            urlConnection.setRequestProperty("app_key",app_key);

            // read the output from the server
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            return stringBuilder.toString();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return e.toString();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        String def;
        try
        {
            JSONObject js = new JSONObject(s);
            JSONArray results = js.getJSONArray("results");

            JSONObject lEntries = results.getJSONObject(0);
            JSONArray laArray = lEntries.getJSONArray("lexicalEntries");

            JSONObject entries = laArray.getJSONObject(0);
            JSONArray e = entries.getJSONArray("entries");

            JSONObject jsonObject = e.getJSONObject(0);
            JSONArray sensesArray = jsonObject.getJSONArray("senses");

            JSONObject d = sensesArray.getJSONObject(0);
            JSONArray de = d.getJSONArray("definitions");

            def = de.getString(0);

            t1.setText(def);
            Toast.makeText(context, def,Toast.LENGTH_SHORT).show();



        }catch(JSONException e )
        {
            e.printStackTrace();
        }

    }
}


