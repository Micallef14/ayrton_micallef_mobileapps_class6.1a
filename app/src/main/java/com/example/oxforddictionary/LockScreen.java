package com.example.oxforddictionary;

import android.content.Intent;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Directions.ShowType;
import com.nightonke.blurlockview.Eases.EaseType;
import com.nightonke.blurlockview.Password;

public class LockScreen extends AppCompatActivity {

    private BlurLockView blurLockView;
    private ImageView imageView;

    public void PasswordCorrect (View v) {openMainMenu();}

    public void openMainMenu(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.lock_view);

        blurLockView = (BlurLockView) findViewById(R.id.blurLockView);
        blurLockView.setBlurredView(imageView);

        imageView = (ImageView) findViewById(R.id.backgroundImageView);

        blurLockView.setCorrectPassword("7777");
        blurLockView.setLeftButton("LEFT");
        blurLockView.setRightButton("RIGHT");
        blurLockView.setTypeface(Typeface.DEFAULT);
        blurLockView.setType(Password.NUMBER, false);


        blurLockView.setOnLeftButtonClickListener(new BlurLockView.OnLeftButtonClickListener() {
            @Override
            public void onClick() {
                Toast.makeText(LockScreen.this, "LEFT CLICKED", Toast.LENGTH_SHORT).show();
            }
        });

        blurLockView.setOnPasswordInputListener(new BlurLockView.OnPasswordInputListener() {
            @Override
            public void correct(String inputPassword) {
                Toast.makeText(LockScreen.this, "Password correct", Toast.LENGTH_SHORT).show();
                openMainMenu();
                }

            @Override
            public void incorrect(String inputPassword) {
                Toast.makeText(LockScreen.this, "Password incorrect", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void input(String inputPassword) {

            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blurLockView.show(1000, ShowType.FADE_IN, EaseType.EaseInOutBack);
            }
        });


    }
        }
