package com.example.oxforddictionary;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Home extends AppCompatActivity  {

    TextView t1;
    EditText e1;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.definition_search);
        t1 = (TextView) findViewById(R.id.textView2);
        e1 = (EditText)findViewById(R.id.editText);
    }
    public void requestApiButtonClick(View view) {
        MyDictionaryRequest myDictionaryRequest = new MyDictionaryRequest(this,t1);
        myDictionaryRequest.execute(e1.getText().toString());
        //e1.getText().toString();
    }
}